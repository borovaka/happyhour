<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use Carbon\Carbon;
use Happyhour\Happyhour\Happyhour;
use Illuminate\Support\Facades\App;

//Route::get('/', function()
//{
//	return View::make('hello');
//});

Route::get('/','HomeController@showWelcome');

Route::get('/test', function(){

    $hp = App::make('happyhour');
    echo $hp->test();

});