<?php namespace Happyhour\Happyhour;

use Illuminate\Support\ServiceProvider;

class HappyhourServiceProvider extends ServiceProvider
{

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('happyhour/happyhour');
	}

	/**
	 * Register the service provider and Happyhour allias
	 *
	 * @return void
	 */
	public function register()
	{
		//

		$this->app->bindShared('happyhour', function ($app) {
			return new Happyhour();
		});

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('happyhour');
	}

}
