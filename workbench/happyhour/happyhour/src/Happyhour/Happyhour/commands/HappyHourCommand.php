<?php namespace Happyhour\Happyhour\commands;

use Happyhour\Happyhour\InitHappyhour;
use Illuminate\Support\Facades\Cache;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HappyHourCommand extends ScheduledCommand
{

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'happyhour:cache';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Init Happyhour system. Put initial values to the cache';
	private $hr;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(InitHappyhour $hr)
	{
		parent::__construct();
		$this->hr = $hr;


	}

	/**
	 * When a command should run
	 * Clear logs on 00:01
	 * Cache new things on 00:02
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return [
			$scheduler->args(['--clear'])->daily()->hours(00)->minutes(01),
			$scheduler->daily()->hours(00)->minutes(02),
		];
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		if ($this->option('clear')) {
			Cache::forget('happyhour_enabled');
			Cache::forget('happyhour_todayhours');
			Cache::forget('happyhour_bonus_percent');
		} elseif ($this->option('show')) {
			print_r(Cache::get('happyhour_todayhours'));
		} else {
			$this->hr->init();
		}

	}


	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('clear', null, InputOption::VALUE_NONE, 'Clear cache.', null),
			array('show', null, InputOption::VALUE_NONE, 'Show cache.', null),
		);
	}

}
