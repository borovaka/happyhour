<?php namespace Happyhour\Happyhour;


use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

/**
 * Class Happyhour
 * @package Happyhour\Happyhour
 */
class Happyhour implements IHappyhour
{


	public function __construct()
	{

	}

	/**
	 * Retrieves the bonus value of the happy hour
	 * if the Happy Hour is running
	 *
	 * EXAMPLE HH ON: getBonusCredits(100) = 120
	 * EXAMPLE HH OFF: getBonusCredits(100) = 100
	 *
	 * @param int $credits
	 * @return int
	 */
	public function getBonusCredits($credits = 0)
	{
		// TODO: Implement getBonusCredits() method.
		if ($this->isHappyHour()) {
			return intval($credits + ($credits * (self::getTodayBonus() / 100)));
		}

		return $credits;
	}

	/**
	 * Checks to see if there is happy hour
	 * going on at the moment.
	 *
	 * @return bool
	 */
	public function isHappyHour()
	{

		// Check if enabled
		if (self::isEnabledToday()) {
			foreach (self::getTodayHappyHours() as $hour) {
				if (Carbon::now()->between($hour, $hour->copy()->addHour())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Check is Happyhour enabled
	 * @return bool
	 */
	private static function  isEnabledToday()
	{

		return Cache::get('happyhour_enabled') ?: false;

	}

	/**
	 * Get what hours are active for promotion
	 * @return array
	 */
	private static function  getTodayHappyHours()
	{
		return Cache::get('happyhour_todayhours') ?: array();

	}

	/**
	 * Get Bunus percentage val
	 * @return int
	 */
	private static function  getTodayBonus()
	{

		return Cache::get('happyhour_bonus_percent') ?: 0;

	}
}