<?php
/**
 * Created by PhpStorm.
 * User: Borisalv
 * Date: 11/25/2014
 * Time: 8:52 AM
 */

namespace Happyhour\Happyhour;


use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

/**
 * Class InitHappyhour
 * Used for initialization and settings of HeppyHour
 * Must not be used directly
 *
 * @package Happyhour\Happyhour
 */
class InitHappyhour
{

	// Cache lifetime in minutes 1440 = 1day
	const DAY_MINUTES = 1440;

	private $enabled;
	private $bonus;
	private $startDate;
	private $endDate;
	private $duration;
	private $workingHours;
	private $todayHappyHours;


	public function __construct()
	{

	}

	/**
	 * Init vals
	 * @throws HappyhourException
	 */
	public function init()
	{

		$this->enabled = Config::get('happyhour::happyhour_enabled');
		$this->startDate = Carbon::parse(Config::get('happyhour::happyhour_start_date'));
		$this->endDate = Carbon::parse(Config::get('happyhour::happyhour_end_date'));

		if ($this->checkEnabled()) {

			$this->bonus = Config::get('happyhour::happyhour_bonus_percent');
			$this->duration = Config::get('happyhour::happyhour_duration_hours');
			$this->workingHours = Config::get('happyhour::happyhour_working_hours');

		} else {
			return;
		}

		$this->validate();
		$this->choiceTodayHappyHours();
		$this->cacheTodayHappyHours();

	}

	/**
	 * Check DateTime restrictions form config
	 * @return bool
	 */

	private function checkEnabled()
	{
		if (!Carbon::now()->between($this->startDate, $this->endDate) || !$this->enabled) {
			Cache::add('happyhour_enabled', 'false', self::DAY_MINUTES);
			return false;
		}
		Cache::add('happyhour_enabled', 'true', self::DAY_MINUTES);
		return true;
	}

	/**
	 * Basic validation of injected config values
	 *
	 * @throws HappyhourException
	 */
	private function validate()
	{
		if (empty($this->bonus) || !is_int($this->bonus)) throw new HappyhourException('Error in bonus value or not set');
		if (empty($this->duration) || !is_int($this->duration)) throw new HappyhourException('Error in duration value or not set');
		if (empty($this->workingHours) || !is_array($this->workingHours)) throw new HappyhourException('Error in workingHours value or not set');


	}

	/**
	 * Populate $this->todayHappyHours with today promo hours
	 */

	private function choiceTodayHappyHours()
	{

		$tmp = is_array(array_rand($this->workingHours, $this->duration)) ? array_rand($this->workingHours, $this->duration) : [array_rand($this->workingHours, $this->duration)];
		foreach ($tmp as $v)
			$this->todayHappyHours[] = Carbon::create(Carbon::now()->year, Carbon::now()->month, Carbon::now()->day, $this->workingHours[$v]);

	}

	/**
	 * Add vals to cache
	 */
	private function cacheTodayHappyHours()
	{
		Cache::add('happyhour_bonus_percent', $this->bonus, self::DAY_MINUTES);
		Cache::add('happyhour_todayhours', $this->todayHappyHours, self::DAY_MINUTES);
	}


}