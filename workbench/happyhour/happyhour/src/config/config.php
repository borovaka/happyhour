<?php
return array(

	// Happyhour system switch
	'happyhour_enabled' => 'true',

	// Happyhour Bonus percentage
	'happyhour_bonus_percent' => 20,

	// Start Happyhour date
	'happyhour_start_date' => '2014-11-01',

	// End Happyhour  Date
	'happyhour_end_date' => '2014-11-30',

	// Duration in hours -> not grater than 24 and not less then 'happyhour_working_hours' array items
	'happyhour_duration_hours' => 1,

	// Hours to pick from
	'happyhour_working_hours' => [15],
);
